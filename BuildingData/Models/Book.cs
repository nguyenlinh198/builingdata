﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingData.Models
{
    public class Book
    {

        public int BookId { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string CoverImage { get; set; }

    }
    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();

            books.Add(new Book { BookId = 1, Title = "Vulpate", Author = "Futurn", CoverImage = "Assets/1.png" });
            books.Add(new Book { BookId = 2, Title = "Mazin", Author = "Sequiter Que", CoverImage = "Assets/2.png" });
            books.Add(new Book { BookId = 3, Title = "Elit", Author = "Terpor", CoverImage = "Assets/3.png" });
            books.Add(new Book { BookId = 4, Title = "Etian", Author = "Option", CoverImage = "Assets/4.png" });
            books.Add(new Book { BookId = 5, Title = "Feugaint", Author = "Accumsan", CoverImage = "Assets/5.png" });
            books.Add(new Book { BookId = 6, Title = "Nonumery", Author = "Legunt Xaeplus", CoverImage = "Assets/6.png" });
            books.Add(new Book { BookId = 7, Title = "Nostrad", Author = "Eleifend", CoverImage = "Assets/7.png" });
            books.Add(new Book { BookId = 8, Title = "Per Modo", Author = "Voro Tation", CoverImage = "Assets/8.png" });
            books.Add(new Book { BookId = 9, Title = "Suscipit AD", Author = "Jack Tibbles", CoverImage = "Assets/9.png" });
            books.Add(new Book { BookId = 10, Title = "Decima", Author = "Volupat", CoverImage = "Assets/10.png" });
            books.Add(new Book { BookId = 11, Title = "Consequat", Author = "Est Posslm", CoverImage = "Assets/11.png" });
            books.Add(new Book { BookId = 12, Title = "Aliquip", Author = "Magna", CoverImage = "Assets/12.png" });
            books.Add(new Book { BookId = 13, Title = "Vulpate", Author = "Futurn", CoverImage = "Assets/13.png" });
            books.Add(new Book { BookId = 14, Title = "AAA", Author = "AAA", CoverImage = "Assets/14.jpg" });
            return books;
        }
    }
}


